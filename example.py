def add():
    while True:
        a = int(input("Enter a first value: "))
        b = int(input("Enter a second value: "))
        c = int(input("Enter a third value: "))
        print(a + b + c)
        x = str(input("Stop operation? Yes or No: "))
        if x == "No":
            continue
        else:
            break


def sub():
    while True:
        a = int(input("Enter a first value: "))
        b = int(input("Enter a second value: "))
        c = int(input("Enter a third value: "))
        print(a - b - c)
        x = str(input("Stop operation? Yes or No: "))
        if x == "No":
            continue
        else:
            break


def multi():
    while True:
        a = int(input("Enter a first value: "))
        b = int(input("Enter a second value: "))
        c = int(input("Enter a third value: "))
        print(a * b * c)
        x = str(input("Stop operation? Yes or No: "))
        if x == "No":
            continue
        else:
            break


def prog():
    print("""Function:
    1. Addition
    2. Subtraction
    3. Multiplication""")
    while True:
        f1 = int(input("Select number function: "))
        if f1 == 1:
            add()
            continue
        elif f1 == 2:
            sub()
            continue
        elif f1 == 3:
            multi()
            continue
        else:
            print("Error")
            break


prog()

print()
input()

